module goutils

go 1.15

require (
	gorm.io/driver/sqlite v1.1.1
	gorm.io/gorm v1.20.0
)
